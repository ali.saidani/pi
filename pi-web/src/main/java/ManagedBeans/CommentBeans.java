package ManagedBeans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import Entites.Comment;
import Entites.Post;
import Services.PostService;


@ManagedBean(name = "commentBean")
@SessionScoped
public class CommentBeans implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String commentaire;
	private Date datedeCreation;
	private Post post;
	
	
	private Integer postSelectedId;
	private List<Post> posts;
	private List<Comment> comments;
	
	@EJB
	PostService postService;
	
@PostConstruct
public void init()
{
	posts = postService.getAllPosts(); 
}

	
	public String addComment() {
		String navigateTo="null";
		SimpleDateFormat simpleFormat = new SimpleDateFormat("dd-MM-yyyy");
		Comment comment = new Comment(commentaire);
		Post postSelected = new Post();
		postSelected.setPostId(postSelectedId);
		comment.setPost(postSelected);
		postService.ajouterComment(comment);
		try{
	        String host ="smtp.gmail.com" ;
	        String user = "ali.saidani@esprit.tn";
	        String pass = "S11392022";
	        String to = "dalisd259@gmail.com";
	        String from = "ali.saidani@esprit.tn";
	        String subject = "Comment";
	        String messageText = "Hi Dear , you have new comment in your post.";
	        boolean sessionDebug = false;

	        Properties props = System.getProperties();

	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.port", "587");
	        
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.required", "true");

	        java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
	        Session mailSession = Session.getDefaultInstance(props, null);
	        mailSession.setDebug(sessionDebug);
	        Message msg = new MimeMessage(mailSession);
	        msg.setFrom(new InternetAddress(from));
	        InternetAddress[] address = {new InternetAddress(to)};
	        msg.setRecipients(Message.RecipientType.TO, address);
	        msg.setSubject(subject); msg.setSentDate(new Date());
	        msg.setText(messageText);

	         javax.mail.Transport transport=mailSession.getTransport("smtp");
	       transport.connect(host, user, pass);
	       transport.sendMessage(msg, msg.getAllRecipients());
	       transport.close();
	       System.out.println("message send successfully");
	     
	    }
		catch(Exception ex)
	    {
	        System.out.println(ex);
	    }
		navigateTo = "/pages/Post/Postlist.xhtml?faces-redirect=true"; 
		return navigateTo; 
		}
	
		
		



	public String getCommentaire() {
		return commentaire;
	}


	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}


	public Date getDatedeCreation() {
		return datedeCreation;
	}


	public void setDatedeCreation(Date datedeCreation) {
		this.datedeCreation = datedeCreation;
	}


	public Post getPost() {
		return post;
	}


	public void setPost(Post post) {
		this.post = post;
	}


	

	public Integer getPostSelectedId() {
		return postSelectedId;
	}


	public void setPostSelectedId(Integer postSelectedId) {
		this.postSelectedId = postSelectedId;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public PostService getPostService() {
		return postService;
	}

	public void setPostService(PostService postService) {
		this.postService = postService;
	} //	public class ExempleClientHttpAPI {
//	  
//	 private String accessToken;
//	 private String BASE_URL = "https://api.smsmode.com/http/1.6/";
//	  
//	 public ExempleClientHttpAPI(String accessToken) {
//	 super();
//	 this.accessToken = accessToken;
//	 }
//	  
//	 public static void main(String[] args) {
//	 String accessToken = "xxxxYYYYYYAAAAAZZZZZZ";
//	 String message = "My Message €€ éé ££££ ùùùù";
//	 String destinataires = "06XXXXXXXXX,07YYYYYYYY"; //Receivers separated by a comma
//	 String emetteur = "MySenderID"; //Optional parameter, allows to deal with the sms sender
//	 String stopOption = "1"; //Deal with the STOP sms when marketing send (cf. API HTTP documentation)
//	  
//	 ExempleClientHttpAPI client = new ExempleClientHttpAPI(accessToken);
//	  
//	 client.sendSMSUsingGet(message, destinataires, emetteur, stopOption);
//	 client.sendSMSUsingPost(message, destinataires, emetteur, stopOption);
//	 }
//	  
//try {
//	 String getURL = BASE_URL + "sendSMS.do";
//	 GetMethod httpMethod = new GetMethod(getURL);
//	 httpMethod.addRequestHeader("Content-Type", "plain/text; charset=ISO-8859-15");
//	  
//	 NameValuePair params[] = { new NameValuePair("accessToken", this.accessToken), //
//	 new NameValuePair("message", message), //
//	 new NameValuePair("numero", destinataires), //Receivers separated by a comma
//	 new NameValuePair("emetteur", emetteur), //Optional parameter, allows to deal with the sms sender
//	 new NameValuePair("stop", optionStop) //Deal with the STOP sms when marketing send (cf. API HTTP documentation) };
//	  
//	 httpMethod.setQueryString(EncodingUtil.formUrlEncode(params, "ISO-8859-15"));
//	  
//	 System.out.println(httpMethod.getURI() + "" + httpMethod.getQueryString());
//	  
//	 executeMethod(httpMethod);
//	 } catch (Exception e) {
//	 manageError(e);
//	 }
//	 }
//	  
//	 private void executeMethod(HttpMethod httpMethod) throws IOException, HttpException {
//	 HttpClient httpClient = new HttpClient();
//	  
//	 System.out.println(httpMethod);
//	 int codeReponse = httpClient.executeMethod(httpMethod);
//	 verifyReponse(httpMethod, codeReponse);
//	 }
//	  
//	 private void verifyReponse(HttpMethod httpMethod, int codeReponse) throws IOException {
//	 if (codeReponse == HttpStatus.SC_OK || codeReponse == HttpStatus.SC_ACCEPTED) {
//	 String result = new String(httpMethod.getResponseBody());
//	 System.out.println(result);
//	 }
//	 }
//	  
//	 private void manageError(Exception e) {
//	 e.printStackTrace();
//	 System.err.println("Error during API call");
//	 }
//	  
//	 public void sendSMSUsingPost(String text, String destinataires, String emetteur, String optionStop) {
//	 try {
//	 String postURL = BASE_URL + "sendSMS.do";
//	 PostMethod httpMethod = new PostMethod(postURL);
//	 httpMethod.addRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-15");
//	  
//	 NameValuePair data[] = { new NameValuePair("accessToken", this.accessToken), //
//	 new NameValuePair("message", text), //
//	 new NameValuePair("numero", destinataires), //Receivers separated by a comma
//	 new NameValuePair("emetteur", emetteur), //Optional parameter, allows to deal with the sms sender
//	 new NameValuePair("stop", optionStop) //Deal with the STOP sms when marketing send (cf. API HTTP documentation)
//	 };
//	 httpMethod.setRequestBody(data);
//	  
//	 System.out.println("///////////////////////");
//	 httpMethod.getRequestEntity().writeRequest(System.out);
//	 executeMethod(httpMethod);
//	  
//	 } catch (Exception e) {
//	 manageError(e);
//	 }
//	 }
//	}
	
}
