package ManagedBeans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Entites.Comment;
import Entites.Post;
import Services.CommentService;
import Services.PostService;


@ManagedBean(name = "postBean")
@SessionScoped
public class PostBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String content;
	private Date  creationDate;
	private String hashtag;
	private int nbrDislike;
	private int nbrLikes;
	private String nomCandidat;
	private String type;
	private List<Comment> comments;
	public List<Comment> getComments() {
		return comments;
	}



	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}



	public String getCommentaire() {
		return commentaire;
	}



	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	private String commentaire;
	private boolean etat;
	
	
	 // Date currentDate = new Date();
	
	//LocalDateTime today =  LocalDateTime.now();    
//	LocalDateTime week = today.plusMinutes(5);      //today.plusDays
	// LocalDateTime weeks = today.plusMinutes(15); 
	
public boolean isEtat() {
		return etat;
	}



	public void setEtat(boolean etat) {
		this.etat = etat;
	}

@EJB
PostService postService;
CommentService commentService;


public CommentService getCommentService() {
	return commentService;
}



public void setCommentService(CommentService commentService) {
	this.commentService = commentService;
}



public void addPost() {
	postService.ajouterPost(new Post(content,hashtag, type)); }

private List<Post> posts;

public List<Post> getPosts() 
{
	posts = postService.getAllPosts();
	return posts;
}


private int postIdToBeUpdated;

public void updatePost()
{
	postService.updatePost(new Post(postIdToBeUpdated, content,hashtag,type));

}



public void displayPost(Post empl)

{
	this.setPostIdToBeUpdated(empl.getPostId());
	this.setContent(empl.getContent());
	this.setHashtag(empl.getHashtag());
	this.setType(empl.getType()); 
	
	
}


public void removePost(int postId)
{
	postService.deletePostById(postId);
}

public void likeDislike(Post post) { 
	
	
	
	postService.LikeDislike(post);


}


public boolean EtatLikeDislike(Post post)
{
if (postService.showPostDetail(post) == 0) 
{
	etat = false;
	return true;
	
}
etat = true;
return false;
}

public String getContent() {
	return content;
}

public void setContent(String content) {
	this.content = content;
}

public Date getCreationDate() {
	return creationDate;
}

public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
}

public String getHashtag() {
	return hashtag;
}

public void setHashtag(String hashtag) {
	this.hashtag = hashtag;
}

public int getNbrDislike() {
	return nbrDislike;
}

public void setNbrDislike(int nbrDislike) {
	this.nbrDislike = nbrDislike;
}

public int getNbrLikes() {
	return nbrLikes;
}

public void setNbrLikes(int nbrLikes) {
	this.nbrLikes = nbrLikes;
}

public String getNomCandidat() {
	return nomCandidat;
}

public void setNomCandidat(String nomCandidat) {
	this.nomCandidat = nomCandidat;
}

public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

public PostService getPostService() {
	return postService;
}

public void setPostService(PostService postService) {
	this.postService = postService;
}



public Integer getPostIdToBeUpdated() {
	return postIdToBeUpdated;
}



public void setPostIdToBeUpdated(Integer postIdToBeUpdated) {
	this.postIdToBeUpdated = postIdToBeUpdated;
}



public static long getSerialversionuid() {
	return serialVersionUID;
}



}
