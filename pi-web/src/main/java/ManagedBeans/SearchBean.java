package ManagedBeans;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


import Entites.Post;
import Services.PostService;

@ManagedBean(name = "searchBean")
@SessionScoped
public class SearchBean implements Serializable {
private static final long serialVersionUID = 1L;

	private String SearchText;
	private String hash;




	private List<Post> posts;

	

	public String getSearchText() {
		return SearchText;
	}



	public void setSearchText(String searchText) {
		SearchText = searchText;
	}



	public List<Post> getPosts() {
		return posts;
	}



	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}



	public PostService getPostService() {
		return postService;
	}



	public void setPostService(PostService postService) {
		this.postService = postService;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@EJB
	PostService postService;
	
	
	public String goToDisplayPosts(String search) { 
		
		
		String navigateTo = "null"; 
		posts=postService.displayselectedPosts(this.SearchText);
		
			navigateTo = "/pages/Post/Postlist2.xhtml?faces-redirect=true"; 
			return navigateTo; 
	} 

	
	public String getHash() {
		return hash;
	}



	public void setHash(String hash) {
		this.hash = hash;
	}



	public String goToDisplayhashPosts() { 
		
		
		String navigateTo = "null"; 
		posts=postService.displayselectedhashPosts(this.hash);
		
			navigateTo = "/pages/Post/Postlist2.xhtml?faces-redirect=true"; 
			return navigateTo; 
	} 

}
