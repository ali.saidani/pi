package Services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Entites.Comment;
import Interfaces.CommentServiceRemote;


@Stateless
@LocalBean
public class CommentService implements CommentServiceRemote {
	@PersistenceContext
	EntityManager em;
	
	public int ajouterComment(Comment comment) 
	{
		em.persist(comment);
		return comment.getCommentid();
	}
	

}
