package Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entites.Comment;
import Entites.Post;
import Interfaces.PostServiceRemote;


@Stateless
@LocalBean
public class PostService implements PostServiceRemote {
	@PersistenceContext
	EntityManager em;
	
	public int ajouterPost(Post post) 
	{
		em.persist(post);
		return post.getPostId();
	}
	
	
	@Override
	public List<Post> getAllPosts() {
		List<Post> emp = em.createQuery("Select e from Post e",Post.class).getResultList();
		return emp;
	}
	
	@Override
	public void updatePost(Post post) 
	{ 
		Post emp = em.find(Post.class, post.getPostId()); 
		emp.setContent(post.getContent()); 
		emp.setHashtag(post.getHashtag()); 
		emp.setType(post.getType()); 
	}
	
	@Override
	public void deletePostById(int postId) {
		em.remove(em.find(Post.class, postId));
	}


	@Override
	public int ajouterComment(Comment comment) {
		em.persist(comment);
		return comment.getCommentid();
		
		
	}
	
	
	public int showPostDetail(Post post)
	{
		Post emp = em.find(Post.class, post.getPostId()); 
		return emp.getNbrLikes();

	}


	@Override
	public void LikeDislike(Post post) {
		Post emp = em.find(Post.class, post.getPostId()); 
        if(emp.getNbrLikes() == 0 )
        {
            emp.setNbrLikes(post.getNbrLikes()+1);
        }
        else
        {
        	emp.setNbrLikes(post.getNbrLikes()-1) ;
        }
        
		
	}
	
	
	public List<Post> displayselectedPosts(String hashtag) {
		 
		TypedQuery<Post> query =
				em.createQuery("select m FROM Post m WHERE m.hashtag=:hashtag",
						Post.class);
				query.setParameter("hashtag",hashtag);
				List<Post> posts = null;
				try { posts = query.getResultList(); }
				catch (Exception e) { System.out.println("Erreur : " + e); }
				return posts;

	}

	public List<Post> displayselectedhashPosts(String hashtag) {
		 
		TypedQuery<Post> query =
				em.createQuery("select m FROM Post m WHERE m.hashtag=:hashtag",
						Post.class);
				query.setParameter("hashtag",hashtag);
				List<Post> posts = null;
				try { posts = query.getResultList(); }
				catch (Exception e) { System.out.println("Erreur : " + e); }
				return posts;

	}
	

}
