package Entites;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Entreprises database table.
 * 
 */
@Entity
@Table(name="Entreprises")
@NamedQuery(name="Entrepris.findAll", query="SELECT e FROM Entrepris e")
public class Entrepris implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EntrepriseId")
	private int entrepriseId;

	@Column(name="Description")
	private String description;

	@Column(name="Image")
	private String image;

	@Column(name="Nom")
	private String nom;

	@Column(name="Rating")
	private int rating;

	//bi-directional many-to-one association to CompteEntrepris
	@ManyToOne
	@JoinColumn(name="CompteEntrepriseId")
	private CompteEntrepris compteEntrepris;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="entrepris")
	private List<Role> roles;

	public Entrepris() {
	}

	public int getEntrepriseId() {
		return this.entrepriseId;
	}

	public void setEntrepriseId(int entrepriseId) {
		this.entrepriseId = entrepriseId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getRating() {
		return this.rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public CompteEntrepris getCompteEntrepris() {
		return this.compteEntrepris;
	}

	public void setCompteEntrepris(CompteEntrepris compteEntrepris) {
		this.compteEntrepris = compteEntrepris;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setEntrepris(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setEntrepris(null);

		return role;
	}

}