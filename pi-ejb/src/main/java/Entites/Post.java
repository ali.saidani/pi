package Entites;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Posts database table.
 * 
 */
@Entity
@Table(name="Posts")
@NamedQuery(name="Post.findAll", query="SELECT p FROM Post p")
public class Post implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PostId")
	private int postId;

	private String content;

	private Date creationDate;

	private String hashtag;

	@Column(name="nbr_dislike")
	private int nbrDislike;

	@Column(name="nbr_likes")
	private int nbrLikes;

	@Column(name="nom_candidat")
	private String nomCandidat;

	private String type;

	//bi-directional many-to-one association to Comment
	@OneToMany(mappedBy="post",cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	private List<Comment> comments;

	public Post() {
		super();
	}

	public int getPostId() {
		return this.postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getHashtag() {
		return this.hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}

	public int getNbrDislike() {
		return this.nbrDislike;
	}

	public void setNbrDislike(int nbrDislike) {
		this.nbrDislike = nbrDislike;
	}

	public int getNbrLikes() {
		return this.nbrLikes;
	}

	public void setNbrLikes(int nbrLikes) {
		this.nbrLikes = nbrLikes;
	}

	public String getNomCandidat() {
		return this.nomCandidat;
	}

	public void setNomCandidat(String nomCandidat) {
		this.nomCandidat = nomCandidat;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Comment> getComments() {
		return this.comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Comment addComment(Comment comment) {
		getComments().add(comment);
		comment.setPost(this);

		return comment;
	}

	public Comment removeComment(Comment comment) {
		getComments().remove(comment);
		comment.setPost(null);

		return comment;
	}


	public Post(String content, String hashtag, String type) {
		super();
		this.content = content;
		this.creationDate = new Date();
		this.hashtag = hashtag;
		this.type = type;
	}


	public Post(int postId, String content, String hashtag, String type) {
		super();
		this.postId = postId;
		this.content = content;
		this.hashtag = hashtag;
		this.type = type;
	}
	public Post(String content, Date creationDate, String hashtag, int nbrDislike, int nbrLikes, String nomCandidat,
			String type) {
		super();
		this.content = content;
		this.creationDate = creationDate;
		this.hashtag = hashtag;
		this.nbrDislike = nbrDislike;
		this.nbrLikes = nbrLikes;
		this.nomCandidat = nomCandidat;
		this.type = type;
	}


	public Post(int postId, String content, Date creationDate, String hashtag, int nbrDislike, int nbrLikes,
			String nomCandidat, String type) {
		super();
		this.postId = postId;
		this.content = content;
		this.creationDate = creationDate;
		this.hashtag = hashtag;
		this.nbrDislike = nbrDislike;
		this.nbrLikes = nbrLikes;
		this.nomCandidat = nomCandidat;
		this.type = type;
	}


	

}