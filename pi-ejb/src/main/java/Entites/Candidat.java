package Entites;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Candidats database table.
 * 
 */
@Entity
@Table(name="Candidats")
@NamedQuery(name="Candidat.findAll", query="SELECT c FROM Candidat c")
public class Candidat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CandidatId")
	private int candidatId;

	private String certification;

	private String competence;

	private String email;

	private String experience;

	private String introduction;

	private String nom;

	private String password;

	private String prenom;

	private String type;

	//bi-directional many-to-one association to Candidature
	@OneToMany(mappedBy="candidat")
	private List<Candidature> candidatures;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="candidat")
	private List<Message> messages;

	//bi-directional many-to-one association to NotificationC
	@OneToMany(mappedBy="candidat")
	private List<NotificationC> notificationCs;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="candidat")
	private List<Role> roles;

	public Candidat() {
	}

	public int getCandidatId() {
		return this.candidatId;
	}

	public void setCandidatId(int candidatId) {
		this.candidatId = candidatId;
	}

	public String getCertification() {
		return this.certification;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}

	public String getCompetence() {
		return this.competence;
	}

	public void setCompetence(String competence) {
		this.competence = competence;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExperience() {
		return this.experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getIntroduction() {
		return this.introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Candidature> getCandidatures() {
		return this.candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public Candidature addCandidature(Candidature candidature) {
		getCandidatures().add(candidature);
		candidature.setCandidat(this);

		return candidature;
	}

	public Candidature removeCandidature(Candidature candidature) {
		getCandidatures().remove(candidature);
		candidature.setCandidat(null);

		return candidature;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Message addMessage(Message message) {
		getMessages().add(message);
		message.setCandidat(this);

		return message;
	}

	public Message removeMessage(Message message) {
		getMessages().remove(message);
		message.setCandidat(null);

		return message;
	}

	public List<NotificationC> getNotificationCs() {
		return this.notificationCs;
	}

	public void setNotificationCs(List<NotificationC> notificationCs) {
		this.notificationCs = notificationCs;
	}

	public NotificationC addNotificationC(NotificationC notificationC) {
		getNotificationCs().add(notificationC);
		notificationC.setCandidat(this);

		return notificationC;
	}

	public NotificationC removeNotificationC(NotificationC notificationC) {
		getNotificationCs().remove(notificationC);
		notificationC.setCandidat(null);

		return notificationC;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setCandidat(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setCandidat(null);

		return role;
	}

}