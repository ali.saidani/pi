package Entites;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TestEnLignes database table.
 * 
 */
@Entity
@Table(name="TestEnLignes")
@NamedQuery(name="TestEnLigne.findAll", query="SELECT t FROM TestEnLigne t")
public class TestEnLigne implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TestEnLigneid")
	private int testEnLigneid;

	public TestEnLigne() {
	}

	public int getTestEnLigneid() {
		return this.testEnLigneid;
	}

	public void setTestEnLigneid(int testEnLigneid) {
		this.testEnLigneid = testEnLigneid;
	}

}