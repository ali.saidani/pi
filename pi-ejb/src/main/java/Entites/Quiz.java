package Entites;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Quizs database table.
 * 
 */
@Entity
@Table(name="Quizs")
@NamedQuery(name="Quiz.findAll", query="SELECT q FROM Quiz q")
public class Quiz implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Quizid")
	private int quizid;

	public Quiz() {
	}

	public int getQuizid() {
		return this.quizid;
	}

	public void setQuizid(int quizid) {
		this.quizid = quizid;
	}

}