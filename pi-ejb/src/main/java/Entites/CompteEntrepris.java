package Entites;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the CompteEntreprises database table.
 * 
 */
@Entity
@Table(name="CompteEntreprises")
@NamedQuery(name="CompteEntrepris.findAll", query="SELECT c FROM CompteEntrepris c")
public class CompteEntrepris implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CompteEntrepriseId")
	private int compteEntrepriseId;

	@Column(name="Email")
	private String email;

	@Column(name="MotDePasse")
	private String motDePasse;

	//bi-directional many-to-one association to Entrepris
	@OneToMany(mappedBy="compteEntrepris")
	private List<Entrepris> entreprises;

	public CompteEntrepris() {
	}

	public int getCompteEntrepriseId() {
		return this.compteEntrepriseId;
	}

	public void setCompteEntrepriseId(int compteEntrepriseId) {
		this.compteEntrepriseId = compteEntrepriseId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotDePasse() {
		return this.motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public List<Entrepris> getEntreprises() {
		return this.entreprises;
	}

	public void setEntreprises(List<Entrepris> entreprises) {
		this.entreprises = entreprises;
	}

	public Entrepris addEntrepris(Entrepris entrepris) {
		getEntreprises().add(entrepris);
		entrepris.setCompteEntrepris(this);

		return entrepris;
	}

	public Entrepris removeEntrepris(Entrepris entrepris) {
		getEntreprises().remove(entrepris);
		entrepris.setCompteEntrepris(null);

		return entrepris;
	}

}