package Interfaces;

import java.util.List;

import Entites.Comment;
import Entites.Post;



public interface PostServiceRemote {
	 public int ajouterPost(Post post);
	 public List<Post> getAllPosts() ;
	 public void updatePost(Post post);
	 public void deletePostById(int postId);
	 public void LikeDislike(Post post );
     public int ajouterComment(Comment comment);
     public List<Post> displayselectedhashPosts(String hashtag);
}
